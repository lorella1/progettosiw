package it.uniroma3.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.controller.validator.AttivitaValidator;
import it.uniroma3.model.Allievo;
import it.uniroma3.model.Attivita;
import it.uniroma3.model.Centro;
import it.uniroma3.service.AllievoService;
import it.uniroma3.service.AttivitaService;
import it.uniroma3.service.CentroService;

@Controller
public class AttivitaController {

	@Autowired
	private AttivitaService attivitaService;

	@Autowired
	private AllievoService allievoService;
	
	@Autowired
	private CentroService centroService;

	@Autowired
	private AttivitaValidator attivitaValidator;

	@RequestMapping("/addAttivita")
	public String addAttivita(Model model) {
		model.addAttribute("attivita", new Attivita());
		return "attivitaForm";
	}

	@RequestMapping("/listaAttivita")
	public String listaAttivita(Model model) {
		model.addAttribute("listaAttivita", this.attivitaService.findAll());
		return "listaAttivita";
	}

	@RequestMapping(value = "/newAttivita/{id}", method = RequestMethod.POST)
	public String newAttivita(@Valid @ModelAttribute("attivita") Attivita attivita, @PathVariable("id") Long id,
			Model model, BindingResult bindingResult) {
		
		this.attivitaValidator.validate(attivita, bindingResult);
		
		Centro centro = this.centroService.findById(id);
		model.addAttribute("centro", centro);

		if(this.attivitaService.alreadyExists(attivita)) {
			model.addAttribute("exists", "L'attivita' gia' esiste");
			return "attivitaForm";
		}

		if(!bindingResult.hasErrors()) {
			attivita.setCentro(centro);
			this.attivitaService.save(attivita);
			centro.addAttivita(attivita);
			return "mostraAttivita";
		}

		return "attivitaForm";
	}

	@RequestMapping(value = "/attivita/{id}", method = RequestMethod.GET)
	public String getAttivita(@PathVariable("id") Long id, Model model) {
		model.addAttribute("attivita", this.attivitaService.findById(id));
		model.addAttribute("allievo", new Allievo());
		return "iscrizione";
	}
	

	@RequestMapping(value = "/iscrizione/{id}", method = RequestMethod.POST)
	public String iscrizione(@ModelAttribute("allievo") Allievo allievo, @PathVariable("id") Long id, Model model) {
		
		if(!this.allievoService.alreadyExists(allievo)) {
			return "allievoForm";
		}
		
		else {
			Attivita attivita = this.attivitaService.findById(id);
			model.addAttribute("attivita", attivita);
			if(attivita.verificaDisponibilita()) {
			Allievo trovato = this.allievoService.findByNomeAndCognome(allievo.getNome(), allievo.getCognome());
			attivita.addAllievo(trovato);
			trovato.addAttivita(attivita);
			this.attivitaService.save(attivita);
			return "mostraAttivita";
			}
			else {
				model.addAttribute("full", "Disponibilit� esaurita");
				return "iscrizione";
			}
		}
	}
}