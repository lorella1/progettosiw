package it.uniroma3;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import it.uniroma3.model.Allievo;
import it.uniroma3.model.Attivita;
import it.uniroma3.model.Centro;
import it.uniroma3.service.AllievoService;
import it.uniroma3.service.AttivitaService;
import it.uniroma3.service.CentroService;


@SpringBootApplication
public class SiwDemoSpringApplication {

	@Autowired
	private AllievoService allievoService;
	
	@Autowired
	private AttivitaService attivitaService;
	
	@Autowired
	private CentroService centroService;
	
	public static void main(String[] args) {
		SpringApplication.run(SiwDemoSpringApplication.class, args);
	}
	
	@PostConstruct
	public void init() {
		Allievo allievo = new Allievo("Gaia", "Locchi", "17/08/1996", "Roma", "gaialocchi@outlook.it", "3483654688");
		allievoService.save(allievo);
		
		Centro centro = new Centro("Information Technology", "Via della Vasca Navale, 79", "informationtechnology@libero.it", "0645595774", 100);
		centroService.save(centro);
		
		Attivita attivita = new Attivita("Java Developers", "20/06/2018 16:00");
		attivita.setCentro(centro);
		attivitaService.save(attivita);		
	}
}
