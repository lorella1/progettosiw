package it.uniroma3.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Centro {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable=false)
	private String nome;

	private String indirizzo;

	private int capienzaMax;

	private String email;

	private String telefono;

	@OneToMany
	@JoinColumn(name = "centro_id")
	private List<Attivita> listaAttivita;
	
	
	public Centro(String nome, String indirizzo, String email, String telefono, int capienzaMax) {
		this.nome = nome;
		this.indirizzo = indirizzo;
		this.email = email;
		this.telefono = telefono;
		this.capienzaMax = capienzaMax;
	}
	
	public Centro() {
		this.listaAttivita = new ArrayList<Attivita>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public int getCapienzaMax() {
		return capienzaMax;
	}

	public void setCapienzaMax(int capienzaMax) {
		this.capienzaMax = capienzaMax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public List<Attivita> getListaAttivita() {
		return listaAttivita;
	}

	public void setListaAttivita(List<Attivita> listaAttivita) {
		this.listaAttivita = listaAttivita;
	}

	public void addAttivita(Attivita attivita) {
		this.listaAttivita.add(attivita);
	}

}
